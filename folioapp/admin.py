from django.contrib import admin
from .models import Home, About, Profile, Category, Skills, Portfolio
# Register your models here.


# Home Admin

admin.site.register(Home)



# About Admin

class ProfileInline(admin.TabularInline):
    model = Profile
    extra = 1

@admin.register(About)
class AboutAdmin(admin.ModelAdmin):
     inlines = [
        ProfileInline,
    ]



# Skills Admin

class SkillsInline(admin.TabularInline):
    model = Skills
    extra = 2

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
     inlines = [
        SkillsInline,
    ]



    
# Portfolio Admin

admin.site.register(Portfolio)